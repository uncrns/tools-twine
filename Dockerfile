FROM python:3-alpine

# Packages needed to build PIP packages
RUN apk add --update --no-cache python3-dev gcc musl-dev libffi-dev libressl-dev

# Needed dependencies and custom content
RUN pip install --upgrade --no-cache-dir setuptools wheel twine

# Default command
CMD /bin/sh
